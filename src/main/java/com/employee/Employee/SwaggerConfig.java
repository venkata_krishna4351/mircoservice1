package com.employee.Employee;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Component
@EnableSwagger2
public class SwaggerConfig{

    @Bean
    public Docket Branch() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Generator")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.employee.Employee"))
                .paths(PathSelectors.any())
                .build();
    }
}