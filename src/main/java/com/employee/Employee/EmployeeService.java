package com.employee.Employee;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public Employee addEmployee(Employee employee){
        EmployeeEntity employeeEntity = EmployeeEntity.builder()
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .email(employee.getEmail())
                .employeePosition(employee.getEmployeePosition())
                .salary(employee.getSalary())
                .build();

        EmployeeEntity savedData = employeeRepository.save(employeeEntity);

        return Employee.builder()
                .firstName(savedData.getFirstName())
                .lastName(savedData.getLastName())
                .email(savedData.getEmail())
                .employeePosition(savedData.getEmployeePosition())
                .salary(savedData.getSalary())
                .build();
    }
}
