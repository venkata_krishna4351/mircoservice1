package com.employee.Employee;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/employee")
@AllArgsConstructor
@Slf4j
public class EmployeeController {

    EmployeeService employeeService;

    @PostMapping(path = "/add/employee")
    public Employee addEmployee(@RequestBody Employee employee){
        log.info(" Employee Email: {} ", employee.getEmail());
        return employeeService.addEmployee(employee);
    }
}
